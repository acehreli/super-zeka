all: super_zeka

TEST_KAYNAKLARI := \
    denetim.d \
    hakem.d \
    super_zeka.d \

PROGRAM_KAYNAKLARI := \
    ${TEST_KAYNAKLARI} \
    main.d \

TEST_PARCALARI := ${TEST_KAYNAKLARI:%.d=%.o}
PROGRAM_PARCALARI := ${PROGRAM_KAYNAKLARI:%.d=%.o} birim_testleri.o

DMD_SATIRI := dmd -w -de

testler_tamam: ${TEST_KAYNAKLARI} Makefile
	${DMD_SATIRI} ${TEST_KAYNAKLARI} -unittest -main -ofbirim_testleri
	./birim_testleri
	touch testler_tamam

super_zeka: testler_tamam ${PROGRAM_KAYNAKLARI}
	${DMD_SATIRI} ${PROGRAM_KAYNAKLARI} -of$@

clean:
	rm -f birim_testleri testler_tamam super_zeka ${PROGRAM_PARCALARI}
