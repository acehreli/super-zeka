/*
 * Süper Zeka Oyunu
 *
 * Ali Çehreli
 * 2010.06 - 2012.04
 *
 * Programın geçmişi için: http://ddili.org/forum/thread/294
 *
 * Programı tamamen yapısal programlamaya uygun olarak yazdım. İçinde hiç
 * nesne yönelimli yöntemler yok. Belki sonra öyle de yazarız... (Sonradan ek:
 * Ama 'abstract' ile en azından çok şekillilik kullanılmış.)
 *
 * Bol bol in, out, ve unittest blokları kullandım.
 *
 * Phobos'tan şu işlevleri kullandım:
 *
 * - randomShuffle: kendisine verilen aralığı rastgele karıştırır
 *
 * - repeat: birinci parametresini ikinci parametresi kadar tekrarlar
 *
 * - std.algorithm.find: birinci parametresi içinde ikinci parametresini arar;
 *   dönüş değeri, bulduğu noktadan sona kadar olan bir aralıktır. Örneğin
 *   find("abcxyz", "c")'nin sonucu "cxyz"dir.
 */

module main;

import std.stdio;
import std.random;
import std.getopt;

import super_zeka;

int main(string[] parametreler)
{
    dstring alfabe = "0123456789abcçdefgğhıijklmnoöpqrsştuüvwxyz";
    size_t haneAdedi = 4;
    size_t rakamAdedi = 10;

    try {
        getopt(parametreler,
               "hane|h", &haneAdedi,
               "rakam|a", &rakamAdedi);

    } catch (Exception hata) {
        writeln("Kullanım:");
        writefln("  %s [--rakam <adet>] [--hane <adet>]", parametreler[0]);
        writeln();
        writeln("Parametreler: ");
        writefln("  --rakam <adet>  Kaç rakamdan seçileceği (varsayılan: %s)",
                rakamAdedi);
        writefln("  --hane  <adet>  Kaç hane tutulacağı (varsayılan: %s)",
                haneAdedi);
        return 0;
    }

    if (rakamAdedi > alfabe.length) {
        writefln("En fazla %s rakam kullanılabilir: %s.",
                alfabe.length, alfabe);
        return 1;
    }

    if (haneAdedi > rakamAdedi) {
        writeln("Hane adedi rakam adedinden fazla olamaz.");
        return 1;
    }

    auto oyun = new SüperZekaOyunu(alfabe[0..rakamAdedi], haneAdedi);
    oyun.oynat();

    return 0;
}
